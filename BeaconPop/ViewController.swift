//
//  ViewController.swift
//  BeaconPop
//
//  Created by Gabriel Theodoropoulos on 10/3/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

import UIKit
import QuartzCore
import CoreLocation
import CoreBluetooth


class ViewController: UIViewController, CBPeripheralManagerDelegate {
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        var statusMessage = ""
        
           switch peripheral.state {
           case CBManagerState.poweredOn:
               statusMessage = "Bluetooth Status: Turned On"
        
           case CBManagerState.poweredOff:
               if isBroadcasting {
                switchBroadcastingState(sender: self)
               }
               statusMessage = "Bluetooth Status: Turned Off"
        
           case CBManagerState.resetting:
               statusMessage = "Bluetooth Status: Resetting"
        
           case CBManagerState.unauthorized:
               statusMessage = "Bluetooth Status: Not Authorized"
        
           case CBManagerState.unsupported:
               statusMessage = "Bluetooth Status: Not Supported"
        
           default:
               statusMessage = "Bluetooth Status: Unknown"
           }
        
           lblBTStatus.text = statusMessage
    }
    

    @IBOutlet weak var btnAction: UIButton!
    
    @IBOutlet weak var lblStatus: UILabel!
    
    @IBOutlet weak var lblBTStatus: UILabel!
    
    @IBOutlet weak var txtMajor: UITextField!
    
    @IBOutlet weak var txtMinor: UITextField!
    
    
    let uuid = NSUUID(uuidString: "0A9CD774-5D6E-4C67-81C4-EEFAAE2E1EF2")
     
    var beaconRegion: CLBeaconRegion!
     
    var bluetoothPeripheralManager: CBPeripheralManager!
     
    var isBroadcasting = false
     
    var dataDictionary = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        btnAction.layer.cornerRadius = btnAction.frame.size.width / 2
        
        let swipeDownGestureRecognizer = UISwipeGestureRecognizer(target: self, action: Selector(("handleSwipeGestureRecognizer:")))
        swipeDownGestureRecognizer.direction = UISwipeGestureRecognizer.Direction.down
        view.addGestureRecognizer(swipeDownGestureRecognizer)
        
        

        bluetoothPeripheralManager = CBPeripheralManager(delegate: self, queue: nil, options: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // MARK: Custom method implementation
    
 @objc func handleSwipeGestureRecognizer(gestureRecognizer: UISwipeGestureRecognizer) {
        txtMajor.resignFirstResponder()
        txtMinor.resignFirstResponder()
    }
    
    
    // MARK: IBAction method implementation
    
    @IBAction func switchBroadcastingState(sender: AnyObject) {
        
        if txtMajor.text == "" || txtMinor.text == "" {
               return
           }
        
        if txtMajor.isFirstResponder || txtMinor.isFirstResponder {
               return
           }
        
        
        if !isBroadcasting {
            if #available(iOS 10.0, *) {
                if bluetoothPeripheralManager.state == CBManagerState.poweredOn {
                    let major: CLBeaconMajorValue = UInt16(txtMajor.text!) ?? 0
                    let minor: CLBeaconMinorValue = UInt16(txtMinor.text!) ?? 0
                    beaconRegion = CLBeaconRegion(proximityUUID: uuid! as UUID, major: major, minor: minor, identifier: "com.appcoda.beacondemo")
                    
                    //  we must advertise the above region, so when a receiver app is nearby to be able to identify the beacon. This is achieved with the next two lines:
                    dataDictionary = beaconRegion.peripheralData(withMeasuredPower: nil)
                    bluetoothPeripheralManager.startAdvertising(dataDictionary as? [String : Any])
                    
                    
                    btnAction.setTitle("Stop", for: UIControl.State.normal)
                    lblStatus.text = "Broadcasting..."
                    
                    txtMajor.isEnabled = false
                    txtMinor.isEnabled = false
                    
                    isBroadcasting = true
                }
                else {
                    
                    bluetoothPeripheralManager.stopAdvertising()
                    
                    btnAction.setTitle("Start", for: UIControl.State.normal)
                    lblStatus.text = "Stopped"
                    
                    txtMajor.isEnabled = true
                    txtMinor.isEnabled = true
                    
                    isBroadcasting = false
                }
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
}

